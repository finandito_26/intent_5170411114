package com.example.inten

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_proses.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val NIM = findViewById<EditText>(R.id.NIM)
        val Nama = findViewById<EditText>(R.id.Nama)
        val Nilai = findViewById<EditText>(R.id.Nilai)

        send.setOnClickListener(){
            if((NIM.text.isEmpty())||(Nama.text.isEmpty())||(Nilai.text.isEmpty())){
                Toast.makeText(applicationContext,"Harus Diisi", Toast.LENGTH_SHORT).show()

            }else if((NIM.text.isNotEmpty())||(Nama.text.isNotEmpty())||(Nilai.text.isNotEmpty())) {
                Toast.makeText(applicationContext, "proses", Toast.LENGTH_LONG).show()

                val NIM = NIM.text.toString()
                val Nama = Nama.text.toString()
                val Nilai = Nilai.text.toString()
                var Keterangan: String? = ""

                val intent = Intent(this, ProsesActivity::class.java)
                intent.putExtra("Nim", NIM)
                intent.putExtra("Nama", Nama)
                intent.putExtra("Nilai", Nilai)
                intent.putExtra("Keterangan", Keterangan)

                startActivity(intent)
            }
        }
    }
}
